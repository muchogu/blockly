### Starting

Run:
docker-compose up

Visit:
http://blockly.localhost/

### Useful Links

https://developers.google.com/blockly/guides/create-custom-blocks/blockly-developer-tools?hl=en

https://stackoverflow.com/questions/46563121/how-to-generate-the-javascript-code-in-blockly

https://www.youtube.com/watch?v=bKMRhUoM0Uc

https://www.youtube.com/watch?v=nXP7OKDCk4Y
