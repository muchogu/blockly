// Blockly.Blocks["tppc_menue_item"] = {
//   init: function () {
//     this.appendValueInput("tppc_menue")
//       .setCheck(null)
//       .appendField("Menue Name:")
//       .appendField(new Blockly.FieldTextInput(""), "menue_name");
//     this.setOutput(true, null);
//     this.setColour(150);
//     this.setTooltip("");
//     this.setHelpUrl("");
//   },
// };

Blockly.common.defineBlocksWithJsonArray([
  {
    type: "tppc_menue",
    message0: "{TPPC Menue %1 %2 }",
    args0: [
      {
        type: "input_dummy",
      },
      {
        type: "input_statement",
        name: "MENUES",
      },
    ],
    output: String,
    colour: 230,
  },
]);

Blockly.common.defineBlocksWithJsonArray([
  {
    type: "tppc_menue_item",
    message0: "TPPC Menue Item %1 %2 %3",
    args0: [
      {
        type: "field_input",
        name: "NAME",
        text: "Menu",
      },
      {
        type: "field_input",
        name: "code",
        text: "Code",
      },
      {
        type: "input_value",
        name: "TPPC_MENU",
      },
    ],
    previousStatement: null,
    nextStatement: null,
    colour: 330,
    tooltip: "",
    helpUrl: "https:",
  },
]);

Blockly.JavaScript["tppc_menue_item"] = function (block) {
  var sub_menue = null;

  var rawCode = Blockly.JavaScript.statementToCode(block, "TPPC_MENU");
  // console.log("Menue Item rawCode: " + rawCode);

  if (rawCode.length != 0) {
    sub_menue = JSON.parse(rawCode);
  }

  var menue_name = block.getFieldValue("NAME");
  var menue_code = block.getFieldValue("code");

  if (menue_code == "Code") {
    menue_code = null;
  }

  return JSON.stringify({
    type: "tppc_menue_item",
    menue_name: menue_name,
    menue_code: menue_code,
    sub_menue: sub_menue,
  });
};

Blockly.JavaScript["tppc_menue"] = function (block) {
  var sub_menue = [];

  var rawCode = Blockly.JavaScript.statementToCode(block, "MENUES");
  // console.log("rawCode:-----" + rawCode + "---:rawCode");

  if (rawCode.length != 0) {
    rawCode = rawCode.replace(/}{/g, "}#-#{");
    const codeArray = rawCode.split("#-#");
    for (let x in codeArray) {
      // console.log(codeArray[x]);
      sub_menue.push(JSON.parse(codeArray[x]));
    }
  }

  return JSON.stringify({ type: "tppc_menue", sub_menue: sub_menue });
};
