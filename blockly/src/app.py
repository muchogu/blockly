from flask import Flask
import os
from flask import Flask, render_template
import graphviz
import pickle
import kanboard
import time
import datetime
from threading import Thread

app = Flask(__name__)


@app.route("/")
def index():

    return render_template('index.html')


if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    app.run(debug=True,host='0.0.0.0',port=port)