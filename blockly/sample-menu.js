while (true) {
  const username = await getInput("Login", "Enter username", 2);
  const password = await getInput("Login", "Enter password.", 3);
  if (username.toString().toLowerCase() === "admin" && password === "1234") {
    break;
  }
  await displayInfo("Login error", "Invalid username or password", 1);
}
while (true) {
  var response = await displayMenuJs([
    "Revenue Collection",
    "Train tickets",
    "Markets",
    "Land rates",
    "Logout",
  ]);
  var logout = false;
  switch (response) {
    case 0:
      response = await displayMenuJs([
        "Routes",
        "Town -> Westlands @ KES50",
        "Town -> Embakassi @KES40",
        "Nairobi -> Nakuru @500",
      ]);
      await handleParking(response);
      break;
    case 1:
      response = await displayMenuJs([
        "Market",
        "Stall Payment - KES 200",
        "Cess Fee - KES 500",
      ]);
      break;
    case 3:
      logout = true;
  }
  if (logout) break;
}

async function handleParking(response) {
  var plate_no = await getInput("Parking", "Enter vehicle plate No.", 2);
  if (plate_no === -1) return;
  plate_no = plate_no.toString().toUpperCase();
  var amount = await getInput("Parking", "Enter amount.", 1);
  if (amount === -1) return;
  if (typeof Android === "undefined") amount = parseInt(amount) / 100;
  const info = await displayInfo(
    "Parking",
    "Are you sure you want to pay KES " +
      amount +
      " for plate No. " +
      plate_no +
      "?",
    2
  );
  var date = new Date().toLocaleDateString();
  var time = new Date().toLocaleTimeString();

  var mode = await displayMenuJs(["Payment", "MPESA", "Visa/Mastercard"]);
  switch (mode) {
    case 0:
      var phoneNumber = await getInput("Parking", "Enter phone number:", 2);
      if (phoneNumber === -1) return;
      var serverResponse = JSON.parse(
        await callRpc(
          JSON.stringify({
            method: "doMpesaCheckout",
            phoneNumber,
            amount,
          })
        )
      );
      const resp = await processPrintJS([
        "^logo.png",
        "$AGENCY TEST MUHIMA",
        "$KIGALI, Rwanda",
        "$Agency Test POS",
        "n----------------------------------------------------",
        "nTID:" + getSerial() + "   MID:RW0010002",
        "n==========================",
        "nDATE: " + date + "   TIME:" + time,
        "nRRN: " + serverResponse.rrn,
        "nSTAN:0000000001      BATCH: 001",
        "n==========================",
        "$        ******" +
          (parseInt(serverResponse.status) !== 0 ? "DECLINED" : "APPROVED") +
          "******",
        "$" + serverResponse.message,
        "n==========================",
        "$Academic Bridge BILL",
        "$PAYMENT",
        "n              UGX " + amount,
        "n              Plate No. " + plate_no,
        "n==========================",
        "nYou were served by.---",
        "$               THANK YOU",
        "$                 BprT2.6.6",
        "#QR me a beer",
        "%barcode works!",
        "p",
      ]);
      break;
    default:
      const info = await displayInfo(
        "Error",
        "Payment mode not implemented",
        1
      );
  }
}
